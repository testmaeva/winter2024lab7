public class Card{
	private String suit;
	private int value;

	public Card(String suit, int value){
		this.suit = suit;
		this.value = value; //rank
	}

	public String getSuit (){
		return this.suit;
	}
	public int getValue (){ //rank
		return this.value;
	}

	public String toString(){ //rank
		String valueToString = String.valueOf(value);

		if(value==1){valueToString = "Ace";}
		if(value==11){valueToString = "Jack";}
		if(value==12){valueToString = "Queen";}
		if(value==13){valueToString = "King";}

		return valueToString+ " of " +suit;
	}
	
	public double calculateScore(){
		double score = value;
		if (this.suit.equals("Hearts")){score+=0.4;}
		if (this.suit.equals("Spades")){score+=0.3;}
		if (this.suit.equals("Diamonds")){score+=0.2;}
		if (this.suit.equals("Clubs")){score+=0.1;}
		return score;
	}
}