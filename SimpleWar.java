import java.util.Random;
import java.util.Scanner;
public class SimpleWar{
    public static void main(String[] args) {
		Scanner reader = new Scanner(System.in);
		
		Deck deck = new Deck();
		deck.shuffle();
		System.out.println("Hey, bring a partner and let u go to Simple War");
		System.out.println("Rules are simple:" +"\n"+
		" There are 52cards in this deck" +"\n"+
		" Deck gets shuffled" +"\n"+
		" For each round, 2cards will b drawn; one for u, one for ur buddy " +"\n"+
		" To win the round, u must have the higer card" +"\n"+
		" The higher card is determined this way;" +"\n"+
		"	"+ "Whichever card has the higest rank(number)" +"\n"+
		"	"+ "Whichever card has the higest rank(number)" +"\n"+
		"	"+ "If both cards have the same rank, the best suit will decide" +"\n"+
		"	"+ "In order: Hearts, Spades, Diamonds, Clubs" +"\n"+
		" U will keep playing until the deck is done and whoever has the most points accumulated wins");
		
		String winner = "";
		int playerOnePoints = 0;
		int playerTwoPoints = 0;

		final String BLACK = "\u001B[30m";
		final String BACKGROUND = "\u001B[47m";
		final String RESET = "\033[0m";

		

		
		while(deck.length()!=0){
			//player one
			Card playerOneCard = deck.drawTopCard();
			//player two
			Card playerTwoCard = deck.drawTopCard();
			
			
			if(playerOneCard.calculateScore()>playerTwoCard.calculateScore()){
				playerOnePoints++;
				winner = "Player One";
			}
			else{
				playerTwoPoints++;
				winner = "Player Two";
			}
			
			System.out.println("-----------"+"\n"+"	"+BLACK+BACKGROUND+ "Player One: " + playerOneCard.toString()+
			"	"+ "Player Two: " + playerTwoCard.toString()+RESET);
			System.out.println("+1point" +"\n"+ winner+ " wins this round");
			System.out.println("Current score" +"\n"+"	"+
			"Player One: " + playerOnePoints+"	"+
			"Player Two: " + playerTwoPoints);
		}
		
		if(playerOnePoints>playerTwoPoints)
			winner = "Player One";
		else
			winner = "Player Two";
		System.out.println("\n"+BACKGROUND+BLACK+ "Congrats " +winner+ ", u won!" +RESET);
    }
}